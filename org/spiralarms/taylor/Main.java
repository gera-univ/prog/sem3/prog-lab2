package org.spiralarms.taylor;

import java.security.InvalidParameterException;

public class Main {

    public static double computeTaylorSum(int x, int precision) {
        double base = 1.0;
        double diff = 1.0;
        double eps = Math.pow(10, -1*precision);
        double i = 1.0;
        while (Math.abs(diff) >= eps) {
            diff *= (x * x) / (i * (i + 1));
            i += 2;
            base += diff;
        }
        return base;
    }

    public static void main(String[] args) {
        try {
            int x = Integer.parseInt(args[0]);
            int precision = Integer.parseInt(args[1]);
            if (precision < 0) {
                throw new InvalidParameterException();
            }
            double myResult = computeTaylorSum(x, precision);
            double standardResult = Math.cosh(x);
            double delta = myResult - standardResult;
            System.out.format("x\t=\t%d\nk\t=\t%d\nres:\t%."+precision+"f\nstd:\t%."+precision+"f\n\u0394\t=\t%.20f\n", x, precision, myResult, standardResult, delta);

        } catch (ArrayIndexOutOfBoundsException e) {
            printUsage();
        } catch (NumberFormatException e) {
            System.out.println("Integer parameters expected!");
            printUsage();
        } catch (InvalidParameterException e) {
            System.out.println("Precision must not be negative!");
        }
    }

    public static void printUsage() {
        System.out.println("Usage: java " + System.getProperty("sun.java.command") + " [x] [precision]");
    }
}
